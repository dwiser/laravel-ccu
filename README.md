# Consume the XML Feed for Cosmo (CCU) in a fluid "Laravel" way
This package makes consuming information from the Cosmo XML feed more fluid and much easier. No need to deal with any XML nonsense.

## Links
* [Installation](#installation)
* [Configuration](#configuration)
* [Updating](#updating)
* [Search Factory](#search-factory)
* [Search Usage](#scopes)
  * [Agent Scope](#ccu-agents)
    * [Dialogs](#ccu-dialogs)
  * [Group Scope](#ccu-groups)
  * [Queue Scope](#ccu-queues)
* [Console Scripts](#scripts)
* [Bug Reports](#bug-reports)

<a name="installation"></a>
## Installation
[Composer](https://getcomposer.org/) is required. You can install the package by editing your composer.json as such:

```json
{
    "repositories": [
        {
            "url": "https://bitbucket.1and1.org/scm/ti/laravel-ccu.git",
            "type": "git"
        }
    ]
}
```

Then you can run composer require:
```bash
composer require ionos/ccu
```

Hopefully that works. I haven't tested it from scratch so you might need to tweak your setup.

<a name="configuration"></a>
## Configuration
You can configure the credentials and endpoints editing your `.env` (or your published `config/ccu.php`) as such:
```
CCU_API_HOST='Primary Hostname'
CCU_API_USER='Primary Username'
CCU_API_PASS='Primary Password'
CCU_API_HOST_2='Secondary Hostname'
CCU_API_USER_2='Secondary Username'
CCU_API_PASS_2='Secondary Password'
```

The configuration can then be published manually or by using the [automated](#init-script) script.
```bash
php artisan vendor:publish --provider="IONOS\\CCU\\ServiceProvider" --tag="config"
php artisan ionos:ccu-init
```

<a name="updating"></a>
## Updating
Due to the simplicity of Composer packages, just run composer update to pull updated code from git.
```bash
composer update
```

<a name="search-factory"></a>
## Accessing the CCU Search Factory:
The Search Factory can be scoped to individual objects which will return an [Illuminate\Support\Collection](https://laravel.com/docs/5.8/collections) of models with all of the expected Collection methods like `filter`, `chunk`, `where`, and `map`. In example documentation, this class is stored in the $factory variable.
```php
>>> $facade = CCU::search();
>>> $class = app(IONOS\CCU\Manager::class)->search();
>>> $alias = app('ionos.ccu')->search();
```

<a name="scopes"></a>
## Search Scopes and Usage
<a name="ccu-agents"></a>
### Agents
An agent is a user which has their own login credentials to the Cosmo system on either `tenant1` or `tenant2`. The collection of agents can then be iterated over. Conceptually: An `Agent` *BelongsTo* a `Group`.
#### Agent Search Scope
```php
>>> $agents = $factory->agents()->get();
>>> get_class($agents);
=> "Illuminate\Support\Collection"
```
#### Common Filters
```php
>>> $supervisors = $agents->filter(function($agent) { return $agent->is_supervisor; });
=> Illuminate\Support\Collection {4569}
>>> $agents_on_chat = $agents->filter(function($agent) { return $agent->is_active && $agent->dialog->is_chat; });
=> Illuminate\Support\Collection {5695
     all: [
       468 => IONOS\CCU\Models\Agent {6872},
       937 => IONOS\CCU\Models\Agent {5207},
     ],
   }
>>> $agent = $agents->where('uname', 'dawiser');
=> IONOS\CCU\Models\Agent {4568}
```
#### Common Properties
```php
>>> $agent->toArray(); // Get "friendly" attributes
=> [
    "id" => 88613,
    "group" => IONOS\CCU\Models\Group {6864},
    "updated_at" => Carbon\Carbon @1552333073 {7375
      date: 2019-03-11 19:37:53.0 UTC (+00:00),
    },
    "fname" => "Daniel",
    "lname" => "Wiser",
    "name" => "Daniel Wiser",
    "uname" => "dawiser",
    "is_online" => false,
    "is_available" => false,
    "is_active" => false,
    "state_id" => 0,
    "time_logged_in" => null,
    "time_in_call" => null,
    "time_in_state" => null,
  ]
```
[Additional Details Here](docs/agents.md)

<a name="ccu-dialogs"></a>
#### Agent Dialogs
The XML Feed does not provide any call or dialog data. All of the information available in the Dialog Class is extrapolated from the agent state information.
```php
>>> $dialog = $agent->dialog;
=> IONOS\CCU\Models\Dialog {5747}
>>> $dialog->exists;
=> true
>>> $agent->dialog->toArray()
=> [
     "type" => "Internet",
     "agent_id" => 88613,
     "queue" => "MEX_Applications_PSA_Hosting_Chat_1st",
     "answered_at" => Carbon\Carbon @1552601931 {10390
       date: 2019-03-14 22:18:51.0 UTC (+00:00),
     },
     "duration" => Carbon\CarbonInterval {6866
       interval: + 00:06:03.0,
     },
   ]
```
[Additional Details Here](docs/dialogs.md)

<a name="ccu-groups"></a>
### Groups
Agents belong to a single group which can contain many agents. Conceptually: A `Group` *HasMany* `Agents`, and `Agents` *BelongTo* a `Group`.
Additionally, a Group can be assigned to many Queues. Conceptually: A `Group` *belongsToMany* `Queues`.
#### Group Search Scope
```php
>>> $groups = $factory->groups()->get();
>>> get_class($groups);
=> "Illuminate\Support\Collection"
```
#### Common Filters
```php
>>> $groups_in_market = $groups->filter(function($group) { return substr( $group->name, 0, 2 ) === "US"; });
>>> $groups_in_market->count();
=> 73
>>> $group_from_name->agents->count()
=> 37
>>> $group_from_name->queues->count()
=> 33
>>> $group = $groups->where('name', 'US_Applications_SMB_Chesterbrook')->first();
=> IONOS\CCU\Models\Queue {5271}
```
#### Common Properties
```php
>>> $group->toArray();
=> [
    "id" => 850,
    "agents" => Illuminate\Support\Collection {7371},
    "queues" => Illuminate\Support\Collection {7372},
    "updated_at" => Carbon\Carbon @1552580737 {3302
      date: 2019-03-14 16:25:37.905082 UTC (+00:00),
    },
    "name" => "US_Applications_Education_Chesterbrook",
  ]
```
[Additional Details Here](docs/groups.md)

<a name="ccu-queues"></a>
### Queues
When a customer calls, their call is routed to a specific Queue. A number of agent groups can be assigned to take calls in that queue.

#### Queue Search Scope
You can scope a search query to show queues using the `->queues()->get()` method on the [search factory](#accessing-the-ccu-search-factory). A collection of $queues is returned.
```php
>>> $queues = $factory->queues()->get();
>>> get_class($queues);
=> "Illuminate\Support\Collection"
```
#### Common Filters
Note: All queues are returned on every request as there's no way to provide a filter to the remote XML Service. This means that the $queues collection will always contain every possible queue. These common filters are applied on the whole queue collection.
```php
>>> $queues_in_market = $queues->filter(function($queue) { return substr( $queue->name, 0, 2 ) === "US"; });
>>> $queues_in_market->count();
=> 73
>>> $queue = $queues->where('name', 'US_Applications_Server_1st')->first();
=> IONOS\CCU\Models\Queue {5271}
```
#### Common Properties
```php
>>> $queue->toArray();
=> [
     "id" => 1466,
     "groups" => Illuminate\Support\Collection {7202},
     "updated_at" => Carbon\Carbon @1552592852 {3308
        date: 2019-03-14 19:47:32.0 UTC (+00:00),
      },
     "name" => "US_Applications_Server_1st",
     "is_active" => true,
     "calls_being_handled" => 1,
     "calls_waiting" => 2,
     "longest_wait" => Carbon\CarbonInterval {16050
       interval: + 00:07:34.0,
     },
   ]
```

#### Common Filters
Note: All queues are returned on every request as there's no way to provide a filter to the remote XML Service. This means that the $queues collection will always contain every possible queue. These common filters are applied on the whole queue collection.
```php
>>> $queue_from_name = $queues->where('name', 'US_Applications_Server_1st')->first();
=> IONOS\CCU\Models\Queue {5271}
>>> $queues_in_market = $queues->filter(function($queue) { return substr( $queue->name, 0, 2 ) === "US"; });
>>> $queues_in_market->count();
=> 73
```

[Additional Details Here](docs/queues.md)


<a name="scripts"></a>
## Console scripts
There are three artisan commands published in this package.
<a name="init-script"></a>
### Initialization Script ([IONOS\CCU\Commands\Init](src\Commands\Init.php))
Checks for configuration files and published them if they don't exist. Will not overwrite existing configuration files.
```bash
php artisan ionos:ccu-init
```
<a name="sync-script"></a>
### Synchronization Script ([IONOS\CCU\Commands\Sync](src\Commands\Sync.php))
To synchronize the current states with tables in the database. Agent state data is overwritten and no history is kept.
```bash
php artisan ionos:ccu-sync --agents
php artisan ionos:ccu-sync --skills
php artisan ionos:ccu-sync --groups
php artisan ionos:ccu-sync --queues
php artisan ionos:ccu-sync --dialogs // Not yet implemented
```
To help with updates less than 1 minute, there is a `--sleep` option which can be set to the number of seconds to delay the script from starting. For example, the 4 following lines could be added as a cron job to sync agent states every 15 seconds.
```bash
php artisan ionos:ccu-sync --agents
php artisan ionos:ccu-sync --agents --sleep=15
php artisan ionos:ccu-sync --agents --sleep=30
php artisan ionos:ccu-sync --agents --sleep=45
```
Finally, there's visual aid when debugging or manually running this script. The `--status` option will set the verbosity of the script and will print a status bar to the console.
```bash
php artisan ionos:ccu-sync --agents --status
```

<a name="clean-script"></a>
### Clean Script ([IONOS\CCU\Commands\Clean](src\Commands\Clean.php))
Cleans up expired data from the database. When an agent or group is deleted, they are completely removed from the CCU XML. To prevent these users from remaining in the database, we should run the cleanup command to soft delete users by setting the `->deleted_at` property or column in the table. By default, we wait 12 hours until we consider something deleted. As it might be best not to delete user data for historical statistics reasons, their information is retained. For *GDPR* reasons, we might want to consider masking the `->uname` property of deleted agents. Uncomment [this line](src/Commands/Clean.php#95) to enable this feature.
```bash
php artisan ionos:ccu-cleanup --hours=12
```

<a name="archive-script"></a>
### Archive Script ([IONOS\CCU\Commands\Archive](src\Commands\Archive.php))
To archive and keep a record of all agent and queue states.
_Documentation Pending_
```bash
php artisan ionos:ccu-archive
```

<a name="bug-reports"></a>
## Bug Reports
Bugs can be reported in [our JIRA queue](https://hosting-jira.1and1.org/browse/CCWAYNE).
Please be aware that the original developer of this project (Daniel Wiser) is no longer with this company so this project should not be considered production ready until someone else takes ownership of it.
