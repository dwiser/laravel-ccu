<?php
return [
  /*
  |--------------------------------------------------------------------------
  | Default Sync Configuration
  |--------------------------------------------------------------------------
  |
  | Here you may specify what configuration options to use when
  | running the application.
  |
  */
  'settings' => [
    'database' => [

      /*
      |--------------------------------------------------------------------------
      | Default Database Connection
      |--------------------------------------------------------------------------
      |
      | Here you may specify the database connection to use when storing result models.
      |
      */
      'connection' => 'mysql',

      /*
      |--------------------------------------------------------------------------
      | Default Table names
      |--------------------------------------------------------------------------
      |
      | Here you may specify what tables to use when storing result models.
      |
      */
      'tables' => [
        'agents' => 'ccu_agents',
        'groups' => 'ccu_groups',
        'queues' => 'ccu_queues',
        'skills' => 'ccu_skills',
        'dialogs' => 'ccu_dialogs',
        'release_codes' => 'ccu_release_codes',
        'group_queue' => 'ccu_group_queue',
        'agent_skill' => 'ccu_agent_skill'
      ]
    ],
  ],

  /*
  |--------------------------------------------------------------------------
  | Default Connection Name
  |--------------------------------------------------------------------------
  |
  | Here you may specify which of the CCU connections below you wish
  | to use as your default connection for all work.
  |
  */
  'default' => env('CCU_CONNECTION', 'default'),

  /*
  |--------------------------------------------------------------------------
  | Connections
  |--------------------------------------------------------------------------
  |
  | This array stores the connections that are available from CCU. You can add
  | as many connections as you like.
  |
  | The key is the name of the connection you wish to use and the value is
  | an array of configuration settings.
  |
  */
  'connections' => [
      'default' => [
          /*
          |--------------------------------------------------------------------------
          | Connection Settings
          |--------------------------------------------------------------------------
          |
          | This connection settings array is directly passed into the CCU manager
          | constructor.
          |
          */
          'settings' => [
              /*
              |--------------------------------------------------------------------------
              | API HOSTNAMES
              |--------------------------------------------------------------------------
              |
              | The hostnames option is an array of servers located on your network that
              | serve the CCU XML API. You can insert as many servers or as little as
              | you'd like.
              |
              | These can be IP addresses of your server(s), or the host name.
              |
              */
              'host' => env('CCU_API_HOST', 'ccspeweb01.domain.com'),
              /*
              |--------------------------------------------------------------------------
              | Timeout
              |--------------------------------------------------------------------------
              |
              | The timeout option allows you to configure the amount of time in
              | seconds that your application waits until a response
              | is received from your CCU API server.
              |
              */
              'timeout' => env('CCU_API_TIMEOUT', 5),
              /*
              |--------------------------------------------------------------------------
              | NTLM Usernames & Passwords
              |--------------------------------------------------------------------------
              |
              | When connecting to the API server, a username and password is required
              | to be able to view filters on your server(s). You can use any CCU
              | account that has admin or supervisor permissions.
              |
              */
              'user' => env('CCU_API_USER', 'user@ccspeweb01.domain.com'),
              'pass' => env('CCU_API_PASS', 'password'),
              /*
              |--------------------------------------------------------------------------
              | SSL Verification
              |--------------------------------------------------------------------------
              |
              | Sets the SSL certificate verification behavior of every request
              |
              | Set to true to enable SSL certificate verification and use the default CA bundle provided by operating system.
              | Set to false to disable certificate verification (this is insecure!).
              | Set to a string to provide the path to a CA bundle to enable verification using a custom certificate.
              |
              */
              'ssl_verification' => env('CCU_API_SSL_VERIFICATION', false),
          ],
      ],
    'secondary' => [
        /*
        |--------------------------------------------------------------------------
        | Connection Settings
        |--------------------------------------------------------------------------
        |
        | This connection settings array is directly passed into the CCU manager
        | constructor.
        |
        */
        'settings' => [
            /*
            |--------------------------------------------------------------------------
            | API HOSTNAMES
            |--------------------------------------------------------------------------
            |
            | The hostnames option is an array of servers located on your network that
            | serve the CCU XML API. You can insert as many servers or as little as
            | you'd like.
            |
            | These can be IP addresses of your server(s), or the host name.
            |
            */
            'host' => env('CCU_API_HOST_2', 'ccspeweb02.domain.com'),
            /*
            |--------------------------------------------------------------------------
            | Timeout
            |--------------------------------------------------------------------------
            |
            | The timeout option allows you to configure the amount of time in
            | seconds that your application waits until a response
            | is received from your CCU API server.
            |
            */
            'timeout' => env('CCU_API_TIMEOUT', 5),
            /*
            |--------------------------------------------------------------------------
            | NTLM Usernames & Passwords
            |--------------------------------------------------------------------------
            |
            | When connecting to the API server, a username and password is required
            | to be able to view filters on your server(s). You can use any CCU
            | account that has admin or supervisor permissions.
            |
            */
            'user' => env('CCU_API_USER_2', 'user@ccspeweb01.domain.com'),
            'pass' => env('CCU_API_PASS_2', 'password'),
            /*
            |--------------------------------------------------------------------------
            | SSL Verification
            |--------------------------------------------------------------------------
            |
            | Sets the SSL certificate verification behavior of every request
            |
            | Set to true to enable SSL certificate verification and use the default CA bundle provided by operating system.
            | Set to false to disable certificate verification (this is insecure!).
            | Set to a string to provide the path to a CA bundle to enable verification using a custom certificate.
            |
            */
            'ssl_verification' => env('CCU_API_SSL_VERIFICATION', false),
        ],
    ],
  ],
];
