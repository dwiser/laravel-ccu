# CCU agents
An agent is a user which has their own login credentials to the Cosmo system on either `tenant1` or `tenant2`. The collection of agents can then be iterated over.

## Fetching all agents and their current states from the configured XML feed:
```php
>>> $agents = app('ionos.ccu')->search()->agents()->get();
>>> get_class($agents);
=> "Illuminate\Support\Collection"
```
## Agent Searches
```php
>>> $first_agent = $agents->first();
=> IONOS\CCU\Models\Agent {4567}
>>> $agent_from_uname = $agents->where('uname', 'dawiser');
=> IONOS\CCU\Models\Agent {4568}
>>> $agent_from_function = $agents->filter(function($agent) { return $agent->id > 88612 && $agent->id < 88614; })->first();
=> IONOS\CCU\Models\Agent {4569}
```

## Getting all attributes from a single agent object
```php
>>> $agent->getAttributes(); // Get RAW attributes
=> [
     "id" => "88613",
     "fn" => "Daniel",
     "ln" => "Wiser",
     "desc" => "US_Applications_Education_Chesterbrook\n",
     "un" => "dawiser",
     "sk" => "US_1u1_WH_Technical_OF(90)",
     "tm" => "",
     "bic" => "False",
     "bldc" => "False",
     "blc" => "False",
     "bc" => "",
     "isag" => "True",
     "isad" => "True",
     "issu" => "True",
     "group" => IONOS\CCU\Models\Group {8508},
     "dialog" => IONOS\CCU\Models\Dialog {8303},
     "updated_at" => Carbon\Carbon @1552326682 {8602
       date: 2019-03-11 17:51:22.0 UTC (+00:00),
     },
   ]
>>> $agent->toArray(); // Get "friendly" attributes
=> [
     "id" => 88613,
     "group" => IONOS\CCU\Models\Group {8300},
     "dialog" => [
       "type" => null,
       "queue" => null,
       "answered_at" => null,
       "duration" => null,
     ],
     "fname" => "Daniel",
     "lname" => "Wiser",
     "name" => "Daniel Wiser",
     "uname" => "dawiser",
     "is_online" => false,
     "is_available" => false,
     "is_active" => false,
     "state_id" => 0,
     "time_in_state" => null,
     "time_logged_in" => null,
   ]
>>> $agent->toJson(); // Agent object in JSON
=> "{"id":88613,"group":{"id":850,"agents":[],"queues":[],"name":"US_Applications_Education_Chesterbrook"},"dialog":{"type":null,"queue":null,"answered_at":null,"duration":null},"fname":"Daniel","lname":"Wiser","name":"Daniel Wiser","uname":"dawiser","is_online":false,"is_available":false,"is_active":false,"state_id":0,"time_in_state":null,"time_logged_in":null}"
```

## Individual attributes can also be obtained through more "friendly" accessors.
### Agent ID: (integer)
```php
>>> $agent->id
=> 88613
```
### Agent First Name: (string)
```php
>>> $agent->fname
=> "Daniel"
```
### Agent Last Name: (string)
```php
>>> $agent->lname
=> "Wiser"
```
### Agent Name: (string)
```php
>>> $agent->name
=> "Daniel Wiser"
```
### Agent Username: (string)
```php
>>> $agent->uname
=> "dawiser"
```
### Agent Skills: ([Illuminate\Support\Collection](https://laravel.com/docs/5.8/collections))
A collection of Skills are returned. Each skill in the collection is stored as an object class with a name and priority property.
```php
>>> $agent->skills
=> Illuminate\Support\Collection {8496
     all: [
       {3311
         +"name": "US_1u1_WH_Technical_OF",
         +"priority": 90,
       },
     ],
   }
>>> $agent->skills->filter(function($skill) { return $skill->priority > 80; })
=> Illuminate\Support\Collection {3815
    all: [
      {3316
        +"name": "US_1u1_WH_Technical_OF",
        +"priority": 90,
      },
    ],
  }
```
### Current release code of agent: (string|null)
```php
>>> $agent->release_code
=> ""
```
### Online status of agent: (boolean)
```php
>>> $agent->is_online
=> false
```
### Offline status of agent: (boolean)
```php
>>> $agent->is_offline
=> true
```
### Available status of agent: (boolean)
```php
>>> $agent->is_available
=> false
```
### Unavailable status of agent: (boolean|null)
```php
>>> $agent->is_unavailable
=> true
```
### Active status of agent: (boolean)
```php
>>> $agent->is_active
=> false
```
### Inactive status of agent: (boolean|null)
```php
>>> $agent->is_inactive
=> true
```
### Agent State ID: (integer)
```php
>>> $agent->state_id
=> 0
```
### Agent State Name: (string)
```php
>>> $agent->state
=> "Not Logged In"
```
### Time that the agent logged in: ([Carbon\Carbon](https://carbon.nesbot.com/docs/))
```php
>>> $agent->time_logged_in
=> Carbon\Carbon @1552329945 {3302
     date: 2019-03-11 18:45:45.0 UTC (+00:00),
   }
```
### Time that the agent has been in their current state: ([Carbon\CarbonInterval](https://carbon.nesbot.com/docs/#api-interval))
```php
>>> $agent->time_in_state
=> Carbon\CarbonInterval {8495
     interval: 0s,
   }
```
### If agent is Administrator: (boolean)
```php
>>> $agent->is_administrator
=> true
```
### If agent is Supervisor: (boolean)
```php
>>> $agent->is_supervisor
=> true
```
### If agent is an unprivileged agent: (boolean)
```php
>>> $agent->is_agent
=> true
```
### The group class that an agent belongs to: ([IONOS\CCU\Models\Group](groups.md))
```php
>>> $agent->group
=> true
```

### The dialog (call or chat) of the agent: ([IONOS\CCU\Models\Dialog](dialogs.md))
```php
>>> $agent->dialog
=> IONOS\CCU\Models\Dialog {5747}
```
