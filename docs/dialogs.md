## Agent Dialogs
The XML Feed does not provide any call or dialog data. All of the information available in the Dialog Class is extrapolated from the agent state information. Due to chained methods, all agents will have a dialog class even if there is no active dialog for the agent. The `->exists` property indicates if the dialog actually exists or if it's just an empty class.
```php
>>> $dialog = $agent->dialog;
=> IONOS\CCU\Models\Dialog {5747}
>>> $dialog->exists;
=> true
```
## Getting all properties of a single dialog object
```php
>>> $agent->dialog->getAttributes()
=> [
     "exists" => true,
     "agent" => IONOS\CCU\Models\Agent {5648},
     "cct" => "0",
     "cq" => "MEX_Applications_PSA_Hosting_Chat_1st",
     "tp" => "363",
     "updated_at" => Carbon\Carbon @1552602294 {3295
       date: 2019-03-14 22:24:54.0 UTC (+00:00),
     },
   ]
>>> $agent->dialog->toArray()
=> [
     "type" => "Internet",
     "agent_id" => 88613,
     "queue" => "MEX_Applications_PSA_Hosting_Chat_1st",
     "answered_at" => Carbon\Carbon @1552601931 {10390
       date: 2019-03-14 22:18:51.0 UTC (+00:00),
     },
     "duration" => Carbon\CarbonInterval {6866
       interval: + 00:06:03.0,
     },
   ]
```

## Individual attributes can also be obtained through more "friendly" accessors.
### Dialog ID: (string)
Currently, there's no way to obtain the call ID from the XML feed. Eventually this may be possible but not at this time. It will always return null for now.
```php
>>> $dialog->id
=> null
```
### Existence: (boolean)
```php
>>> $dialog->exists
=> true
```
### Agent ID: (integer)
```php
>>> $dialog->agent_id
=> 88613
```
### Agent ID: (integer)
```php
>>> $dialog->agent_id
=> 88613
```
### Queue: (string)
```php
>>> $dialog->queue
=> "MEX_Applications_PSA_Hosting_Chat_1st"
```
### If dialog is a chat: (boolean)
```php
>>> $dialog->is_chat
=> true
```
### If dialog is a call: (boolean)
```php
>>> $dialog->is_call
=> false
```
### The duration of the dialog: ([Carbon\CarbonInterval](https://carbon.nesbot.com/docs/#api-interval))
```php
>>> $dialog->duration
=> Carbon\CarbonInterval {6836
     interval: + 00:06:03.0,
   }
```
### When the dialog was answered: ([Carbon\Carbon](https://carbon.nesbot.com/docs/))
```php
>>> $dialog->answered_at
=> Carbon\Carbon @1552601931 {5747
     date: 2019-03-14 22:18:51.0 UTC (+00:00),
   }
```
