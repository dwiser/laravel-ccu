# CCU groups
Agents belong to a single group which can contain many agents. Conceptually: A `Group` *HasMany* `Agents`, and `Agents` *BelongTo* a `Group`.
Additionally, a Group can be assigned to many Queues. Conceptually: A `Group` *belongsToMany* `Queues`.

## Fetching all groups and the relationships between `Agents` and `Queues` from the configured XML feed:
```php
>>> $groups = app('ionos.ccu')->search()->groups()->get();
>>> get_class($groups);
=> "Illuminate\Support\Collection"
```
## Group Searches
```php
>>> $first_group = $groups->first();
=> IONOS\CCU\Models\Group {3323}
>>> $group_from_name = $groups->where('name', 'US_Applications_Education_Chesterbrook')->first();
=> IONOS\CCU\Models\Group {7370}
>>> $us_groups_from_function = $groups->filter(function($group) { return substr( $group->name, 0, 2 ) === "US"; });
>>> $us_groups_from_function->count();
=> 73
```

## Getting all attributes from a single group object
```php
>>> $group->getAttributes()
=> [
     "id" => 850,
     "n" => "US_Applications_Education_Chesterbrook",
     "agents" => Illuminate\Support\Collection {7371},
     "queues" => Illuminate\Support\Collection {7372},
     "updated_at" => Carbon\Carbon @1552580737 {3302
       date: 2019-03-14 16:25:37.905082 UTC (+00:00),
     },
     "name" => "US_Applications_Education_Chesterbrook",
   ]
>>> $group->toArray();
=> [
    "id" => 850,
    "agents" => Illuminate\Support\Collection {7371},
    "queues" => Illuminate\Support\Collection {7372},
    "updated_at" => Carbon\Carbon @1552580737 {3302
      date: 2019-03-14 16:25:37.905082 UTC (+00:00),
    },
    "name" => "US_Applications_Education_Chesterbrook",
  ]
  >>> $group->toJson();
  => "{"id":850,"agents":[{"id":71035,"fname":"Jason","lname":"Mumford","name":"Jason Mumford","uname":"","is_online":false,"is_available":false,"is_active":false,"dialog":[],"state_id":0,"time_in_state":null,"time_logged_in":null},{"id":71209,"fname":"Megan","lname":"Otto","name":"Megan Otto","uname":"","is_online":false,"is_available":false,"is_active":false,"dialog":[],"state_id":0,"time_in_state":null,"time_logged_in":null},{"id":71211,"fname":"Brett","lname":"Neally","name":"Brett Neally","uname":"","is_online":false,"is_available":false,"is_active":false,"dialog":[],"state_id":0,"time_in_state":null,"time_logged_in":null},{"id":71526,"fname":"Kasia","lname":"Gipson","name":"Kasia Gipson","uname":"","is_online":false,"is_available":false,"is_active":false,"dialog":[],"state_id":0,"time_in_state":null,"time_logged_in":null},{"id":79252,"fname":"Robert","lname":"Packard","name":"Robert Packard","uname":"","is_online":false,"is_available":false,"is_active":false,"dialog":[],"state_id":0,"time_in_state":null,"time_logged_in":null},{"id":83446,"fname":"Steven","lname":"Wagner","name":"Steven Wagner","uname":"","is_online":false,"is_available":false,"is_active":false,"dialog":[],"state_id":0,"time_in_state":null,"time_logged_in":null},{"id":88613,"fname":"Daniel","lname":"Wiser","name":"Daniel Wiser","uname":"","is_online":false,"is_available":false,"is_active":false,"dialog":[],"state_id":0,"time_in_state":null,"time_logged_in":null}],"queues":[{"id":493},{"id":500},{"id":505},{"id":517},{"id":528},{"id":542},{"id":547},{"id":551},{"id":553},{"id":555},{"id":562},{"id":609},{"id":1070},{"id":1451},{"id":1452},{"id":1454},{"id":1463},{"id":1619},{"id":1705},{"id":1706},{"id":1711},{"id":1715},{"id":1716},{"id":1061},{"id":1063},{"id":1117},{"id":1467},{"id":1618}],"updated_at":{"date":"2019-03-14 16:35:00.942004","timezone_type":3,"timezone":"UTC"},"name":"US_Applications_Education_Chesterbrook"}"
```

## Individual attributes can also be obtained through more "friendly" accessors.
### Group ID: (integer)
```php
>>> $group->id
=> 850
```
### Group Name: (string)
```php
>>> $group->name
=> "US_Applications_Education_Chesterbrook"
```
### Group Updated At: ([Carbon\Carbon](https://carbon.nesbot.com/docs/))
This is not the literal time the group configuration was last updated. This is just the time that the group was last returned from the XML feed and updated in the local cache.
```php
>>> $group->updated_at
=> Carbon\Carbon @1552581300 {3302
     date: 2019-03-14 16:35:00.942004 UTC (+00:00),
   }
```
### Group Agents: ([Illuminate\Support\Collection](https://laravel.com/docs/5.8/collections))
This is a collection of agents which belong to this group. Because the XML feed does not provide state details about these agents at this location in the tree, only the basic configuration details are provided about the agents. ** DO NOT TRUST STATE DATA RETURNED HERE. ** Use the [->agents()](#ccu-agents) scope to find that information.
```php
>>> $group->agents                            
=> Illuminate\Support\Collection {7371       
     all: [                                   
       IONOS\CCU\Models\Agent {7429},        
       IONOS\CCU\Models\Agent {7430},        
       IONOS\CCU\Models\Agent {7431},        
       IONOS\CCU\Models\Agent {7432},        
       IONOS\CCU\Models\Agent {7433},        
       IONOS\CCU\Models\Agent {7434},        
       IONOS\CCU\Models\Agent {7435},        
     ],                                       
   }
>>> $agent->toArray()
=> [
    "id" => 71035,
    "fname" => "Jason",
    "lname" => "Mumford",
    "name" => "Jason Mumford",
    "uname" => "",
    "is_online" => false,
    "is_available" => false,
    "is_active" => false,
    "dialog" => [],
    "state_id" => 0,
    "time_in_state" => null,
    "time_logged_in" => null,
  ]                                      
```
### Group Queues: ([Illuminate\Support\Collection](https://laravel.com/docs/5.8/collections))
This is a collection of the queues that have been assigned to the selected Group. Like the `->agents` collection, the `->queues` collection does not contain extra data including which includes the `->groups` collection when the parent scope was for groups. It is however, possible to filter the parent $groups collection by the queues that are assigned to them. See the last line below.
```php
>>> $group->queues->count()
=> 28      
>>> $group->queues->first()->toArray()
=> [
     "id" => 493,
     "groups" => Illuminate\Support\Collection {7375},
     "name" => "DefaultQueue",
     "is_active" => true,
   ]
>>> $groups->filter(function($group) { return $group->queues->contains(function($queue) { return $queue->name = "DefaultQueue"; }); })->count();
=> 170                            
```
Additionally, there can be active and inactive assignments between groups and queues. This is indicated by the `->is_active` property on each of the queue in the collection. To return ONLY the active queues, the `->activeQueues()` method is also available on each $group object.
```php
>>> $group->queues->count();
=> 28
>>> $group->activeQueues()->count();
=> 23
```
Also the activeQueues() method is really just shorthand for a collection filter:
```php
/**
 * Get only the active Queues on this group.
 *
 * @return Collection
 */
public function activeQueues()
{
  return $this->queues->filter(function($queue) {
    return $queue->is_active;
  });
}
```

You can also review the [source](../src/Models/Group.php) for all the methods on the queue class.
