# CCU Queues
When a customer calls, their call is routed to a specific Queue. A number of agent groups can be assigned to take calls in that queue.

## Queue Search Scope
You can scope a search query to show queues using the `->queues()->get()` method on the [search factory](#accessing-the-ccu-search-factory). A collection of $queues is returned.
```php
>>> $queues = $factory->queues()->get();
>>> get_class($queues);
=> "Illuminate\Support\Collection"
```

## Common Filters
Note: All queues are returned on every request as there's no way to provide a filter to the remote XML Service. This means that the $queues collection will always contain every possible queue. These common filters are applied on the whole queue collection.
```php
>>> $queue_from_name = $queues->where('name', 'US_Applications_Server_1st')->first();
=> IONOS\CCU\Models\Queue {5271}
>>> $queues_in_market = $queues->filter(function($queue) { return substr( $queue->name, 0, 2 ) === "US"; });
>>> $queues_in_market->count();
=> 73
```

## All Properties
```php
>>> $queue->getAttributes()
=> [
     "id" => 1466,
     "n" => "US_Applications_Server_1st",
     "wt" => "1717",
     "ch" => "1",
     "cwt" => "356",
     "ct" => "0",
     "co" => "4",
     "ca" => "1",
     "cw" => "1",
     "awt" => "1717",
     "act" => "829",
     "cbh" => "1",
     "catqos" => "0",
     "dlro" => "79",
     "groups" => Illuminate\Support\Collection {7202},
     "updated_at" => Carbon\Carbon @1552592109 {3302
       date: 2019-03-14 19:35:09.0 UTC (+00:00),
     },
     "name" => "US_Applications_Server_1st",
     "is_active" => true,
   ]
>>> $queue->toArray();
=> [
     "id" => 1466,
     "groups" => Illuminate\Support\Collection {7202},
     "updated_at" => Carbon\Carbon @1552592852 {3308
        date: 2019-03-14 19:47:32.0 UTC (+00:00),
      },
     "name" => "US_Applications_Server_1st",
     "is_active" => true,
     "calls_being_handled" => 1,
     "calls_waiting" => 2,
     "longest_wait" => Carbon\CarbonInterval {16050
       interval: + 00:07:34.0,
     },
   ]                                                  
```

## Individual attributes can also be obtained through more "friendly" accessors.
### Queue ID: (integer)
```php
>>> $queue->id
=> 1466
```
### Queue Name: (string)
```php
>>> $queue->name
=> "US_Applications_Server_1st"
```
### Queue Updated At: ([Carbon\Carbon](https://carbon.nesbot.com/docs/))
This is not the literal time the queue configuration was last updated. This is just the time that the queue was last returned from the XML feed and updated in the local cache.
```php
>>> $queue->updated_at
=> Carbon\Carbon @1552592852 {3308
     date: 2019-03-14 19:47:32.0 UTC (+00:00),
   }
```
### Calls being handled: (integer)
```php
>>> $queue->calls_being_handled
=> 1
```
### Calls waiting: (integer)
```php
>>> $queue->calls_waiting
=> 2
```
### Calls waiting: (integer)
```php
>>> $queue->calls_waiting
=> 2
```
### Longest Wait: ([Carbon\CarbonInterval](https://carbon.nesbot.com/docs/#api-interval))
In most cases, the seconds or minutes of the interval time are needed. The `->totalSeconds` (integer) or `->totalMinutes` (float) will store the number.
```php
>>> $queue->longest_wait
=> Carbon\CarbonInterval {15753
     interval: + 00:04:44.0,
   }
>>> $queue->longest_wait->totalSeconds
=> 284
>>> $queue->longest_wait->totalMinutes
=> 4.7333333333333
```

You can also review the [source](../src/Models/Queue.php#96-219) for all the methods on the queue class.
