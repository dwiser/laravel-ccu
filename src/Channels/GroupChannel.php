<?php

namespace IONOS\CCU\Channels;

use App\Models\Organization\User;

 /**
  * GroupChannel broadcast class.
  *
  * Example usage in Laravel's routes/channels.php
  *  Broadcast::channel('ccu.group.{group_id}', \IONOS\CCU\Channels\GroupChannel::class);
  *
  */
class GroupChannel
{
  /**
   * Create a new channel instance.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Authenticate the user's access to the channel.
   *  If they are part of the group, they can join the group.
   *
   * @param  \IONOS\CCU\Models\Agent $agent
   * @param  \IONOS\CCU\Models\Group $group
   * @return array|bool
   */
  public function join(Agent $agent, Group $group)
  {
    return $agent->group->id === $group->id;
  }
}
