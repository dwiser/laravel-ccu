<?php

namespace IONOS\CCU\Commands;

use Illuminate\Support\Collection;
use Illuminate\Console\Command;

class Archive extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'ionos:ccu-archive
      {--truncate : Truncate historical data.}
      {--compress : Compress historical data.}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Archive and keep a history of states.';


  /**
   * The LDAP Service.
   *
   * @var CCU Service Provider
   */
  protected $service;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();

    $this->service = app('ionos.ccu');
  }


  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->info('This command is not yet implemented. Nothing was done.');
  }
}
