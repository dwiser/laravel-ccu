<?php

namespace IONOS\CCU\Commands;

use DB;
use Illuminate\Support\Str;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Output\OutputInterface;

class Clean extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'ionos:ccu-clean
    {--status : Show visual status indicators.}
	  {--hours=12 : How long to wait until something should be soft deleted.}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Soft delete old data that hasn\'t been updated within specified hours.';

  /**
   * The default verbosity of command.
   *
   * @var int
   */
  protected $verbosity = OutputInterface::VERBOSITY_QUIET;

  /**
   * The LDAP Service.
   *
   * @var CCU Service Provider
   */
  protected $service;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();

    $this->service = app('ionos.ccu');
  }


  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    $this->setOutputVerbosity();

    $this->info('Marking items deleted if they have not been updated in ' . $this->option('hours') . ' hours.');

    $this->scanAgents();
    $this->scanGroups();
    $this->scanQueues();

  }

  /**
   * Only display output if the status option is provided.
   *
   * @return void
   */
  private function setOutputVerbosity() {
    if( $this->option('status') ) {
      $this->setVerbosity(OutputInterface::VERBOSITY_NORMAL);
    }
  }
  /**
   * Scan all the agents and soft delete those who haven't been updated in $hours.
   *
   * @return void
   */
  private function scanAgents() {
    $expired_agents = DB::connection(config('ccu.settings.database.connection'))
      ->table(config('ccu.settings.database.tables.agents'))
      ->whereNull('deleted_at')
      ->whereDate('updated_at', '<', now()->subHours($this->option('hours')));

    $deleted_agents = $expired_agents->update([
//      'uname' => (string) Str::uuid();
      'deleted_at' => now()
    ]);

    $this->info(PHP_EOL . 'Marked ' . $deleted_agents . ' agents as "deleted".');
  }

  /**
   * Scan all the groups and soft delete those which haven't been updated in $hours.
   *
   * @return void
   */
  private function scanGroups() {
    $expired_groups = DB::connection(config('ccu.settings.database.connection'))
      ->table(config('ccu.settings.database.tables.groups'))
      ->whereNull('deleted_at')
      ->whereDate('updated_at', '<', now()->subHours($this->option('hours')));

    $deleted_groups = $expired_groups->update(['deleted_at' => now()]);

    $this->info(PHP_EOL . 'Marked ' . $deleted_groups . ' Groups as "deleted".');
  }


  /**
   * Scan all the queues and soft delete those which haven't been updated in $hours.
   *
   * @return void
   */
  private function scanQueues() {
    $expired_queues = DB::connection(config('ccu.settings.database.connection'))
      ->table(config('ccu.settings.database.tables.queues'))
      ->whereNull('deleted_at')
      ->whereDate('updated_at', '<', now()->subHours($this->option('hours')));

    $deleted_queues = $expired_queues->update(['deleted_at' => now()]);

    $this->info(PHP_EOL . 'Marked ' . $deleted_queues . ' Queues as "deleted".');
  }
}
