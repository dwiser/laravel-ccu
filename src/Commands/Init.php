<?php

namespace IONOS\CCU\Commands;

use Artisan;
use Illuminate\Support\Collection;
use Illuminate\Console\Command;

class Init extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'ionos:ccu-init';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Initialize CCU Service.';


  /**
   * The LDAP Service.
   *
   * @var CCU Service Provider
   */
  protected $service;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();

    $this->service = app('ionos.ccu');
  }


  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
    if( $this->configsPublished() ) {
      Artisan::call('vendor:publish --provider="IONOS\CCU\ServiceProvider"');

      if( $this->configsPublished() ) {
        $this->info('Package configuration has been published.');
      } else {
        $this->error('There was a problem publishing the configuration.');
      }
    }
  }

  /**
   * Publish Configs.
   *
   * @return boolean
   */
  private function configsPublished()
  {
    $published = is_array( config('ccu') );

    if( !$published ) {
      $this->error('Configuration has not been published.');
    }

    return $published;
  }
}
