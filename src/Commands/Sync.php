<?php

namespace IONOS\CCU\Commands;

use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Output\OutputInterface;

class Sync extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'ionos:ccu-sync
    {--agents : Sync agents.}
    {--skills : Sync skills.}
    {--groups : Sync groups.}
    {--queues : Sync queues.}
    {--dialogs : Sync dialogs. Not yet implemented.}
    {--status : Show visual status indicators.}
    {--sleep=0 : How long to sleep before starting.}';
  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Sync current states to database.';
  /**
   * The default verbosity of command.
   *
   * @var int
   */
  protected $verbosity = OutputInterface::VERBOSITY_QUIET;

  /**
   * The LDAP Service.
   *
   * @var CCU Service Provider
   */
  protected $service;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();

    $this->service = app('ionos.ccu');
  }


  /**
   * Execute the console command.
   *
   * @return mixed
   */
  public function handle()
  {
	  $this->sleep();

    $this->setOutputVerbosity();

    $this->syncRequestedObjects();
  }

  /**
   * Delay execution for X seconds since cron cannot be scheduled for sub-minute times.
   *
   * @return void
   */
  private function sleep() {
    sleep($this->option('sleep'));
  }

  /**
   * Only display output if the status option is provided.
   *
   * @return void
   */
  private function setOutputVerbosity() {
    if( $this->option('status') ) {
      $this->setVerbosity(OutputInterface::VERBOSITY_NORMAL);
    }
  }

  /**
   * .
   *
   * @return viod
   */
  private function syncRequestedObjects() {
    if( $this->option('agents') || $this->option('skills') ) {
      $this->syncAgents();
    }

    if( $this->option('groups') ) {
      $this->syncGroups();
    }

    if( $this->option('queues') ) {
      $this->syncQueues();
    }

    if( $this->option('dialogs')) {
      $this->syncDialogs();
    }
  }



  /**
   * Sync all Agents.
   *
   * @return mixed
   */
  public function syncAgents()
  {
    $start = now();

    $skills = DB::connection(config('ccu.settings.database.connection'))
      ->table(config('ccu.settings.database.tables.skills'))
      ->get();
    $release_codes = DB::connection(config('ccu.settings.database.connection'))
      ->table(config('ccu.settings.database.tables.release_codes'))
      ->get();

    $this->info(PHP_EOL . 'Fetching Agents...');
    $agents = $this->service->search()->agents()->get();

    $this->info(PHP_EOL . 'Response Time: ' . $start->diffForHumans(now(), true));

    $this->info(PHP_EOL . 'Processing ' . $agents->count() . ' Agents.');
    $bar = $this->output->createProgressBar($agents->count());

    foreach ($agents as $agent) {
      try {
        $updated_rows = DB::connection(config('ccu.settings.database.connection'))
          ->table(config('ccu.settings.database.tables.agents'))
          ->whereId($agent->id)
          ->update([
            'group_id' => $agent->group_id,
            'state_id' => $agent->state_id,
            'seconds_in_state' => optional($agent->seconds_in_state)->totalSeconds,
            'seconds_in_call' => optional($agent->dialog->duration)->totalSeconds,
            'release_code_id' => $agent->release_code,
            'is_supervisor' => $agent->is_supervisor,
            'updated_at' => $agent->updated_at,
            'deleted_at' => null
          ]);

        if( $updated_rows == 0 ) {
          DB::connection(config('ccu.settings.database.connection'))
            ->table(config('ccu.settings.database.tables.agents'))
            ->insert([
              'id' => $agent->id,
              'uname' => $agent->uname,
              'group_id' => $agent->group->id,
              'state_id' => $agent->state_id,
              'seconds_in_state' => optional($agent->seconds_in_state)->totalSeconds,
              'seconds_in_call' => optional($agent->dialog->duration)->totalSeconds,
              'release_code_id' => $agent->release_code,
              'is_supervisor' => $agent->is_supervisor,
              'updated_at' => $agent->updated_at,
              'deleted_at' => null
            ]);
        }

        if( $this->option('skills') && $agent->skills->isNotEmpty() ) {
          $agent_skills = [];

          foreach( $agent->skills as $skill ) {
            $skill['id'] = optional($skills->where('name', $skill['name'])->first())->id;

              if($skill['id'] == null) {
                $skill['id'] = DB::connection(config('ccu.settings.database.connection'))
                  ->table(config('ccu.settings.database.tables.skills'))
                  ->insertGetId(['name' => $skill['name']]);

                $skills->push((object) [
                  'id' => $skill['id'],
                  'name' => $skill['name']
                ]);
              }

              $agent_skills[] = [
                'agent_id' => $agent->id,
                'skill_id' => $skill['id'],
                'priority' => $skill['priority']
              ];
            }

            DB::connection(config('ccu.settings.database.connection'))
              ->table(config('ccu.settings.database.tables.release_codes'))
              ->whereAgentId($agent->id)
              ->delete();
            DB::connection(config('ccu.settings.database.connection'))
              ->table(config('ccu.settings.database.tables.release_codes'))
              ->insert($agent_skills);
        }
      } catch(\Illuminate\Database\QueryException $exception) {
        // Couldn't submit record.
      }

      $bar->advance();
    }

    $bar->finish();

    $this->info(PHP_EOL . 'Total time to process Agents: ' . $start->diffForHumans(now(), true));

  }

  /**
   * Sync all Groups.
   *
   * @return mixed
   */
  public function syncGroups()
  {
    $start = now();

    $this->info(PHP_EOL . 'Fetching Groups...');
    $groups = $this->service->search()->groups()->get();

    $this->info(PHP_EOL . 'Response Time: ' . $start->diffForHumans(now(), true));

    $this->info(PHP_EOL . 'Processing ' . $groups->count() . ' Groups.');
    $bar = $this->output->createProgressBar($groups->count());

    foreach($groups as $group) {
      $updated_rows = DB::connection(config('ccu.settings.database.connection'))
        ->table(config('ccu.settings.database.tables.groups'))
        ->whereId($group->id)
        ->update([
          'name' => $group->name,
          'updated_at' => $group->updated_at,
          'deleted_at' => null
        ]);

      if( $updated_rows == 0 ) {
        DB::connection(config('ccu.settings.database.connection'))
          ->table(config('ccu.settings.database.tables.groups'))
          ->insert([
            'id' => $group->id,
            'name' => $group->name,
            'created_at' => $group->updated_at,
            'updated_at' => $group->updated_at,
            'deleted_at' => null
          ]);
      }

      DB::connection(config('ccu.settings.database.connection'))
        ->table(config('ccu.settings.database.tables.group_queue'))
        ->where('group_id', $group->id)
        ->delete();

      $group_queues = $group->queues->map(function($queue) use ($group) {
        return [
          'group_id' => $group->id,
          'queue_id' => $queue->id,
          'active' => $queue->is_active
        ];
      })->toArray();

      DB::connection(config('ccu.settings.database.connection'))
        ->table(config('ccu.settings.database.tables.group_queue'))
        ->insert($group_queues);

        $bar->advance();
      }

      $bar->finish();

      $this->info(PHP_EOL . 'Total time to process Groups: ' . $start->diffForHumans(now(), true));
  }


  /**
   * Sync all Queues.
   *
   * @return mixed
   */
  public function syncQueues()
  {
    $start = now();

    $this->info(PHP_EOL . 'Fetching Queues...');
    $queues = $this->service->search()->queues()->get();

    $this->info(PHP_EOL . 'Response Time: ' . $start->diffForHumans(now(), true));

    $this->info(PHP_EOL . 'Processing ' . $queues->count() . ' Queues.');
    $bar = $this->output->createProgressBar($queues->count());

    foreach($queues as $queue) {
      $updated_rows =  DB::connection(config('ccu.settings.database.connection'))
        ->table(config('ccu.settings.database.tables.queues'))
        ->whereId($queue->id)
        ->update([
          'name' => $queue->name,
          'calls_waiting' => $queue->calls_waiting,
          'calls_being_handled' => $queue->calls_being_handled,
          'longest_wait_time' => optional($queue->longest_wait)->totalSeconds,
          'updated_at' => $queue->updated_at,
          'deleted_at' => null
        ]);

      if( $updated_rows == 0) {
        DB::connection(config('ccu.settings.database.connection'))
          ->table(config('ccu.settings.database.tables.queues'))
          ->insert([
            'id' => $queue->id,
            'name' => $queue->name,
            'calls_waiting' => $queue->calls_waiting,
            'longest_wait_time' => optional($queue->longest_wait)->totalSeconds,
            'calls_being_handled' => $queue->calls_being_handled,
            'updated_at' => $queue->updated_at,
            'deleted_at' => null
          ]);
      }
      $bar->advance();
    }

    $bar->finish();

    $this->info(PHP_EOL . 'Total time to process Queues: ' . $start->diffForHumans(now(), true));
  }


  /**
   * Sync all Dialogs.
   *
   * @return mixed
   */
  public function syncDialogs()
  {
    $start = now();

    $this->info(PHP_EOL . 'Fetching Dialogs...');
    $agents = $this->service->search()->agents()->get();
    $this->info(PHP_EOL . 'Response Time: ' . $start->diffForHumans(now(), true));

    $dialogs = $agents->filter(function($agent) {
      return $agent->dialog->exists;
    })->pluck('dialog');

    $this->info(PHP_EOL . 'Processing ' . $dialogs->count() . ' Dialogs.');
    $bar = $this->output->createProgressBar($dialogs->count());

    foreach ($dialogs as $dialog) {
      $updated_rows = DB::connection(config('ccu.settings.database.connection'))
        ->table(config('ccu.settings.database.tables.dialogs'))
        ->where([
          ['agent_id', '=', $dialog->agent_id],
          ['answered_at', '=', $dialog->answered_at]
        ])
        ->update([
          'ended_at' => $dialog->answered_at->copy()->add($dialog->duration),
          'updated_at' => $dialog->updated_at,
        ]);

      if( $updated_rows == 0 ) {
        DB::connection(config('ccu.settings.database.connection'))
          ->table(config('ccu.settings.database.tables.dialogs'))
          ->insert([
            'agent_id' => $dialog->agent_id,
            'answered_at' => $dialog->answered_at,
            'ended_at' => $dialog->answered_at->copy()->add($dialog->duration),
            'updated_at' => $dialog->updated_at,
          ]);
      }

      $bar->advance();
    }

    $bar->finish();

    $this->info(PHP_EOL . 'Total time to process Dialogs: ' . $start->diffForHumans(now(), true));

  }
}
