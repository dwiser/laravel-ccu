<?php

namespace IONOS\CCU\Connections;

use Cache;
use SimpleXMLElement;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use IONOS\CCU\Models\ModelFactory;
use IONOS\CCU\Connections\Response;
use IONOS\CCU\Exceptions\AuthenticationException;
use IONOS\CCU\Exceptions\InvalidFilterException;

class Connection
{
  /**
   * Settings for Guzzle Http client
   *
   * @var Array
   */
  protected $settings;
  /**
   * Authenticated Guzzle Http client
   *
   * @var GuzzleHttp\Client
   */
  protected $client;
  /**
   * Filters of the request query.
   *   Options are 'AConfigs', 'GConfigs', 'QConfigs', 'ACalls', 'QCalls', 'QHistory' and 'SServer'
   *
   * @var Illuminate\Support\Collection
   */
  protected $filters;
  /**
   * The Response object.
   *
   * @var boolean | IONOS\CCU\Connections\Response
   */
  protected $response;

  /**
   * Construct a connection class.
   *
   * @param  Array $settings
   * @return void
   */
  public function __construct($settings)
  {
    $this->settings = $settings;
    $this->clearFilters();

    $this->client = new Client([
      'base_uri' => 'https://' . $this->settings['host'] . '/rtrdll/rtrweb.dll',
      'auth' => [$this->settings['user'], $this->settings['pass'], 'ntlm'],
      'headers' => [
        'Accept' => 'application/xml',
      ],
      'timeout' => $this->settings['timeout'],
      'verify' => $this->settings['ssl_verification'],
      'http_errors' => false,
      'allow_redirects' => false
    ]);
  }





  /**
   * Add a filter to the query.
   *
   * @param String $string
   *
   * @return self
   */
  public function addFilter(string $string)
  {
    if( $this->filters->contains($string) ) {
      return $this;
    }
    $this->filters = $this->filters->push($string);
    $this->response = false;

    return $this;
  }
  /**
   * Set the filters of the query.
   *
   * @param Array $filters
   *
   * @return self
   */
  public function setFilters(array $filters)
  {
    $this->filters = new Collection($filters);

    $this->response = false;

    return $this;
  }
  /**
   * Remove all filters from the query.
   *
   * @return string
   */
  public function clearFilters()
  {
    $this->filters = new Collection;
    $this->response = false;

    return $this;
  }
  /**
   * Get filters on connection.
   *
   * @return Illuminate\Support\Collection
   */
  public function getFilters()
  {
    return $this->filters;
  }
  /**
   * Get URL formatted filters.
   *
   * @return string
   */
  public function getFilterString()
  {
    return $this->filters->implode("+");
  }
  /**
   * If filters have been set.
   *
   * @return boolean
   */
  public function hasFiltersSet()
  {
    return $this->filters->isNotEmpty();
  }





  /**
   * Get the Guzzle client for the request.
   *
   * @return GuzzleHttp\Client
   */
  public function getClient()
  {
    return $this->client;
  }




  /**
   * Make request.
   *
   * @return Self
   */
  public function makeRequest()
  {
    $response = $this->client->request('GET', '', ['query' => ['Filter' => $this->getFilterString()]]);

    if( $response->getStatusCode() == '401' ) {
      throw new AuthenticationException($response->getReasonPhrase(), $this->settings);
    }

    if( $response->getStatusCode() == '200' )
    {
      $xml = new SimpleXMLElement($response->getBody());

      if( $xml->attributes()->msg == 'UNKNOWN FILTER' ) {
        throw new InvalidFilterException($xml->attributes()->msg2);
      }
    }
    $tenant = substr($this->settings['user'], strpos($this->settings['user'], "@") + 1);
    $this->setResponse( new Response( $xml , $tenant ) );

    return $this;
  }
  /**
   * Set response.
   *
   * @param IONOS\CCU\Connections\Response $response
   *
   * @return boolean
   */
  public function setResponse(Response $response)
  {
    $this->response = $response;
  }
  /**
   * Check for cached response.
   *
   * @return boolean
   */
  public function responseIsCached()
  {
    if( true ) { // Caching Enabled
      return Cache::has('https://' . $this->settings['host'] . '/rtrdll/rtrweb.dll?Filter=' . $this->getFilterString());
    }
    return false;
  }
  /**
   * Get response.
   *
   * @return Response
   */
  public function getResponse()
  {
    if( $this->response ) {
      return $this->response;
    }

    return $this->makeRequest()->response;
  }
}
