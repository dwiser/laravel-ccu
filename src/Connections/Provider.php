<?php

namespace IONOS\CCU\Connections;

class Provider
{
    /**
     * The providers connection.
     *
     * @var Connection
     */
    protected $connection;
    /**
     * The providers configuration.
     *
     * @var Configuration
     */
    protected $configuration;

    /**
      * Constructor.
      *
      * @param array $configuration
      * @param Connection $connection
      */
    public function __construct($configuration = [], Connection $connection)
    {
        $this->setConfiguration($configuration)->setConnection($connection);
    }

     /**
      * Sets the current configuration.
      *
      * @param Configuration|array $configuration
      */
    public function setConfiguration($configuration = [])
    {
        $this->configuration = $configuration;
        return $this;
    }

    /**
     * Sets the current connection.
     *
     * @param Connection $connection
     *
     * @return $this
     */
    public function setConnection(Connection $connection = null)
    {
        $this->connection = $connection;
        return $this;
    }
    /**
     * Returns the current configuration instance.
     *
     * @return Array
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }
    /**
     * Returns the current connection instance.
     *
     * @return ConnectionInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }




    /**
     * {@inheritdoc}
     */
    public function make()
    {
        return new ModelFactory(
            $this->search()->newQuery()
        );
    }
    /**
     * {@inheritdoc}
     */
    public function search()
    {
        return new SearchFactory(
            $this->connection,
            $this->schema,
            $this->configuration->get('base_dn')
        );
    }
    /**
     * {@inheritdoc}
     */
    public function auth()
    {
        return $this->getGuard();
    }
    /**
     * {@inheritdoc}
     */
    public function connect($username = null, $password = null)
    {
        // Get the default guard instance.
        $guard = $this->getGuard();
        if (is_null($username) && is_null($password)) {
            // If both the username and password are null, we'll connect to the server
            // using the configured administrator username and password.
            $guard->bindAsAdministrator();
        } else {
            // Bind to the server with the specified username and password otherwise.
            $guard->bind($username, $password);
        }
        return $this;
    }
    /**
     * Prepares the connection by setting configured parameters.
     *
     * @return void
     *
     * @throws \Adldap\Configuration\ConfigurationException When configuration options requested do not exist
     */
    protected function prepareConnection()
    {
        if ($this->configuration->get('use_ssl')) {
            $this->connection->ssl();
        } elseif ($this->configuration->get('use_tls')) {
            $this->connection->tls();
        }
        $options = array_replace(
            $this->configuration->get('custom_options'),
            [
                LDAP_OPT_PROTOCOL_VERSION => $this->configuration->get('version'),
                LDAP_OPT_NETWORK_TIMEOUT => $this->configuration->get('timeout'),
                LDAP_OPT_REFERRALS => $this->configuration->get('follow_referrals')
            ]
        );
        $this->connection->setOptions($options);
    }
}
