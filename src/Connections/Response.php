<?php

namespace IONOS\CCU\Connections;

use SimpleXMLElement;
use Carbon\Carbon;

class Response
{
  CONST FEED_REFRESH_RATE_IN_SECONDS = 10;

  /**
   * When the data was valid
   *
   * @var \Carbon\Carbon
   */
  protected $timestamp;
  /**
   * Time until data expires.
   *   Self::$timestamp + Self::FEED_REFRESH_RATE_IN_SECONDS
   *
   * @var \Carbon\Carbon
   */
  protected $expires;
  /**
   * Raw XML element
   *
   * @var SimpleXMLElement
   */
  protected $xml;
  /**
   * Tenant
   *
   * @var String
   */
  protected $tenant;



  /**
   * Construct a response class.
   *
   * @param   SimpleXMLElement $xml
   * @return void
   */
  public function __construct(SimpleXMLElement $xml, String $tenant)
  {
    $this->xml = $xml->asXML();
    $this->tenant = $tenant;

    $this->timestamp = Carbon::parse( $xml->children()[0]->attributes()->ts );
    $this->expires = $this->timestamp->copy()->addSeconds(self::FEED_REFRESH_RATE_IN_SECONDS);
  }




  /**
   * If the response has expired.
   *
   * @return boolean
   */
  public function notExpired()
  {
    return $this->expires->gt(now());
  }
  /**
   * Get the time of expiration.
   *
   * @return Carbon\Carbon
   */
  public function getExpirationTime()
  {
    return $this->expires;
  }
  /**
   * Get the timestamp of the response.
   *
   * @return Carbon\Carbon
   */
  public function getTimestamp()
  {
    return $this->timestamp;
  }
  /**
   * Get the tenant of the response.
   *
   * @return String
   */
  public function getTenant()
  {
    return $this->tenant;
  }
  /**
   * Format the response as an XML string.
   *
   * @return String
   */
  public function getXml()
  {
    return $this->xml;
  }
}
