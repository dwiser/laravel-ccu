<?php

namespace IONOS\CCU\Exceptions;

use Exception;

class AuthenticationException extends Exception
{
    /**
     * Create a new authentication exception.
     *
     * @param string  $message
     */
    public function __construct($message = 'Unauthenticated', Array $settings)
    {
        parent::__construct($message . ' on server ' . $settings['user'] . '@' . $settings['host'] . '.');
    }
}
