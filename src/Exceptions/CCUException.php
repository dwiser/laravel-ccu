<?php

namespace IONOS\CCU\Exceptions;

use Exception;

class CCUException extends Exception
{
    /**
     * Create a new CCU exception.
     *
     * @param string  $message
     */
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
