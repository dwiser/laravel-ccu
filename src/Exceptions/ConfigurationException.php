<?php

namespace IONOS\CCU\Exceptions;

use RuntimeException;

class ConfigurationException extends RuntimeException
{
    /**
     * Create a new authentication exception.
     *
     * @param string  $message
     */
    public function __construct($message = 'CCU configuration could not be found. Try re-publishing using `php artisan vendor:publish --provider="IONOS\CCU\ServiceProvider"`.')
    {
        parent::__construct($message);
    }
}
