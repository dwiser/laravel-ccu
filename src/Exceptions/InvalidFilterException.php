<?php

namespace IONOS\CCU\Exceptions;

use Exception;

class InvalidFilterException extends Exception
{
    /**
     * Create a new Invalid Filter exception.
     *
     * @param string  $message
     */
    public function __construct($filter)
    {
        parent::__construct('Invalid Filter: "' . $filter . '""');
    }
}
