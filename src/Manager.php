<?php

namespace IONOS\CCU;

use IONOS\CCU\Query\Builder;
use IONOS\CCU\Query\SearchFactory;
use IONOS\CCU\Connections\Connection;
use IONOS\CCU\Exceptions\ConfigurationException;

class Manager
{
  /**
   * The default connection name.
   *
   * @var string
   */
  protected $default = 'default';
  /**
   * The connections.
   *
   * @var array
   */
  protected $connections = [];



  /**
   * Add connections.
   *
   * @param mixed         $connections
   *
   * @return $this
   *
   * @throws \InvalidArgumentException When an invalid type is given as the configuration argument.
   */
  public function addConnections($connections)
  {

    foreach ($connections as $name => $config) {
      // Create a new provider.
      $connection = $this->newConnection($config['settings']);

      // Add the provider to the CCU manager.
      $this->addConnection($connection, $name);
    }

    return $this;
  }
  /**
   * Returns a new Connection instance.
   *
   * @param array $settings
   *
   * @return Connection
   */
  protected function newConnection(array $settings)
  {
    return new Connection($settings);
  }
  /**
   * Add connections.
   *
   * @param Connection    $connection
   * @param string        $name
   *
   * @return $this
   *
   * @throws \InvalidArgumentException When an invalid type is given as the configuration argument.
   */
  public function addConnection(Connection $connection, $name = 'default')
  {
    $this->connections[$name] = $connection;

    return $this;
  }
  /**
   * Returns all of the connections.
   *
   * @return array
   */
  public function getConnections()
  {
    return $this->connections;
  }
  /**
   * Check if multiple connections are configured.
   *
   * @return boolean
   * @throws \RuntimeException
   */
  public function hasConnections()
  {
    $count = count( $this->connections );

    // Verify configuration exists.
    if ( $count == 0 ) {
        throw new ConfigurationException();
    }

    return true;
  }
  /**
   * Check if multiple connections are configured.
   *
   * @return boolean
   */
  public function hasMultipleConnections()
  {
    return count( $this->connections ) > 1;
  }
  /**
   * Retrieves a conection using its specified name.
   *
   * @param string $name
   *
   * @throws CCUException When the specified connection does not exist.
   *
   * @return Connection
   */
  public function getConnection($name)
  {
    if ( !is_null($name) && array_key_exists($name, $this->connections)) {
      return $this->connections[$name];
    }
    throw new CCUException("The connection provider '$name' does not exist.");
  }
  /**
   * Removes a connection by the specified name.
   *
   * @param string $name
   *
   * @return $this
   */
  public function removeConnection($name)
  {
    unset($this->connections[$name]);
    return $this;
  }
  /**
   * Retrieves the first default connection.
   *
   * @throws CCUException When no default connection exists.
   *
   * @return Connection
   */
  public function getDefaultConnection()
  {
    return $this->getConnection($this->default);
  }
  /**
   * Sets the default connection.
   *
   * @param string $name
   *
   * @throws CCUException When the specified provider does not exist.
   */
  public function setDefaultConnection($name = 'default')
  {
    if ($this->getConnection($name) instanceof Connection) {
      $this->default = $name;
    }
  }
  /**
   * setDefaultConnection alias.
   *
   * @param string $name
   *
   * @throws CCUException When the specified provider does not exist.
   *
   * @return $this
   */
  public function on($connection = null)
  {
    $this->setDefaultConnection($connection);
    return $this;
  }



  /**
   * Get search factory.
   *
   * @param  string|null  $connection
   *
   * @return SearchFactory
   */
  public function search()
  {
    $this->hasConnections();

    return new SearchFactory( $this->getConnections() );
  }
}
