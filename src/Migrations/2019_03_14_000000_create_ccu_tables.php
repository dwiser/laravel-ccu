<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCcuTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      if( !Schema::hasTable(config('ccu.settings.database.tables.agents')) ) {
        Schema::create(config('ccu.settings.database.tables.agents'), function (Blueprint $table) {
          $table->integer('id')->unsigned()->unique();
          $table->string('uname')->unique();
          $table->string('group_id');
          $table->integer('state_id')->unsigned()->default(0);
          $table->integer('seconds_in_state')->unsigned()->nullable()->default(0);
          $table->integer('seconds_in_call')->unsigned()->nullable()->default(0);
          $table->integer('release_code_id')->unsigned()->nullable()->default(0);
          $table->boolean('is_supervisor')->default(false);
          $table->timestampsTz();
          $table->softDeletesTz();
        });
      }
      if( !Schema::hasTable(config('ccu.settings.database.tables.groups')) ) {
        Schema::create(config('ccu.settings.database.tables.groups'), function (Blueprint $table) {
          $table->integer('id')->unsigned()->unique();
          $table->string('name')->unique();
          $table->timestampsTz();
          $table->softDeletesTz();
        });
      }
      if( !Schema::hasTable(config('ccu.settings.database.tables.queues')) ) {
        Schema::create(config('ccu.settings.database.tables.queues'), function (Blueprint $table) {
          $table->integer('id')->unsigned()->unique();
          $table->integer('market_id')->unsigned()->nullable();
          $table->string('name')->unique();
          $table->integer('calls_waiting')->unsigned()->default(0);
          $table->integer('longest_wait_time')->unsigned()->default(0);
          $table->integer('calls_being_handled')->unsigned()->default(0);
          $table->timestampsTz();
          $table->softDeletesTz();
        });
      }
      if( !Schema::hasTable(config('ccu.settings.database.tables.skills')) ) {
        Schema::create(config('ccu.settings.database.tables.skills'), function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->unique();
        });
      }
      if( !Schema::hasTable(config('ccu.settings.database.tables.states')) ) {
        Schema::create(config('ccu.settings.database.tables.states'), function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->unique();
          $table->boolean('active')->default(false);
          $table->boolean('available')->default(false);
          $table->boolean('online')->default(false);
        });
      }
      if( !Schema::hasTable(config('ccu.settings.database.tables.dialogs')) ) {
        Schema::create(config('ccu.settings.database.tables.dialogs'), function (Blueprint $table) {
          $table->increments('id');
          $table->string('call_id')->nullable();
          $table->unsignedInteger('agent_id')->nullable();
          $table->timestampTz('answered_at');
          $table->timestampTz('ended_at')->nullable();
          $table->timestampTz('updated_at');
        });
      }
      if( !Schema::hasTable(config('ccu.settings.database.tables.release_codes')) ) {
        Schema::create(config('ccu.settings.database.tables.release_codes'), function (Blueprint $table) {
          $table->increments('id');
          $table->string('name')->unique();
        });
      }


      if( !Schema::hasTable(config('ccu.settings.database.tables.agent_skill')) ) {
        Schema::create(config('ccu.settings.database.tables.agent_skill'), function (Blueprint $table) {
          $table->integer('agent_id')->unsigned();
          $table->integer('skill_id')->unsigned();
          $table->integer('priority')->unsigned();

          $table->foreign('agent_id')->references('id')->on('ccu_agents')->onDelete('cascade');
          $table->foreign('skill_id')->references('id')->on('ccu_skills')->onDelete('cascade');
        });
      }
      if( !Schema::hasTable(config('ccu.settings.database.tables.group_queue')) ) {
        Schema::create(config('ccu.settings.database.tables.group_queue'), function (Blueprint $table) {
          $table->integer('group_id')->unsigned();
          $table->integer('queue_id')->unsigned();
          $table->boolean('active')->default(true);

          $table->foreign('group_id')->references('id')->on('ccu_groups')->onDelete('cascade');
          $table->foreign('queue_id')->references('id')->on('ccu_queues')->onDelete('cascade');
        });
      }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::disableForeignKeyConstraints();

      Schema::dropIfExists(config('ccu.settings.database.tables.agents'));
      Schema::dropIfExists(config('ccu.settings.database.tables.groups'));
      Schema::dropIfExists(config('ccu.settings.database.tables.queues'));
      Schema::dropIfExists(config('ccu.settings.database.tables.skills'));
      Schema::dropIfExists(config('ccu.settings.database.tables.states'));
      Schema::dropIfExists(config('ccu.settings.database.tables.dialogs'));
      Schema::dropIfExists(config('ccu.settings.database.tables.release_codes'));

      Schema::dropIfExists(config('ccu.settings.database.tables.agent_skill'));
      Schema::dropIfExists(config('ccu.settings.database.tables.group_queue'));
    }
}
