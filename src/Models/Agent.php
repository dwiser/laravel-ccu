<?php

namespace IONOS\CCU\Models;

use Carbon\Carbon;
use Sabre\Xml\Reader;
use Jenssegers\Model\Model;
use Illuminate\Support\Collection;

class Agent extends Model
{
  /**
   * The attributes that should be visible in arrays.
   *
   * @var array
   */
  protected $visible = [
    'id', 'fname', 'lname', 'name', 'uname', 'is_online', 'is_available', 'is_active', 'dialog', 'state_id', 'time_in_state', 'time_logged_in', 'dialog', 'group', 'tenant'
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'name' => 'string',
  ];
  /**
   * The accessors to append to the model's array form.
   *
   * @var array
   */
  protected $appends = [
    'id', 'fname', 'lname', 'name', 'uname', 'is_online', 'is_available', 'is_active', 'dialog', 'state_id', 'time_in_state', 'time_logged_in', 'dialog'
  ];



  /**
   * Construct a processor class.
   *
   * @param  array $attributes
   * @return void
   */
  public function __construct(Reader $reader)
  {
    parent::__construct($reader->parseAttributes());

    $children = $reader->parseInnerTree();

    if( !is_null($children) ) {
      foreach($children as $child) {
        if ($child['value'] instanceof Group) {
          $this->group = $child['value'];
        }
      }
    }

    $this->dialog = new Dialog($this);
  }



  /** ==================
   *  Get and cast properties from the raw model attributes.
   *  ==================
   */
  /**
   * Get the agent id.
   *
   * @return integer
   */
  public function getIdAttribute()
  {
    return (int) $this->attributes['id'];
  }
  /**
   * Get the agent id.
   *
   * @return integer
   */
  public function getGroupIdAttribute()
  {
    return optional($this->group)->id;
  }
  /**
   * Get the agents User Name.
   *
   * @return string
   */
  public function getUnameAttribute()
  {
    return (string) $this->un;
  }
  /**
   * Get the agent first name.
   *
   * @return string
   */
  public function getFnameAttribute()
  {
    return (string) $this->fn;
  }
  /**
   * Get the agent last name.
   *
   * @return string
   */
  public function getLnameAttribute()
  {
    return (string) $this->ln;
  }
  /**
   * Get the agent name.
   *
   * @return string
   */
  public function getNameAttribute()
  {
    return (string) $this->fname . " " . $this->lname;
  }
  /**
   * Get the release code of the agent.
   *
   * @return string|null
   */
  public function getReleaseCodeAttribute() {
    return $this->is_offline ? null : (string) $this->rc;
  }
  /**
   * Get the time the agent has been in their current state.
   *
   * @return Carbon\CarbonInterval|null
   */
  public function getTimeInStateAttribute($seconds = false) {
    if( $this->is_offline ) {
      return null;
    }

    if( $this->is_inactive ) {
      return Carbon::now()->diffAsCarbonInterval(Carbon::now()->addSeconds($this->tp, false));
    }

    return Carbon::now()->diffAsCarbonInterval(Carbon::now()->addSeconds($this->ctp, false));
  }
  /**
   * Get the time the agent has been in their current state.
   *  As the time fluctuates 5-10 seconds, we round to the nearest minute.
   *
   * @return Carbon\Carbon|null
   */
  public function getTimeLoggedInAttribute() {
    if( $this->is_offline ) {
      return null;
    }

    return $this->updated_at->subSeconds($this->tl, false);
  }
  /**
   * Get the agents skills.
   *
   * @return Illuminate\Support\Collection
   */
  public function getSkillsAttribute() {
    $skills = explode(',', (string) $this->sk);
    $collection = new Collection;

    foreach ($skills as $key => $skill) {
      if (preg_match('/(\w+)\((\d+)\)/', $skill, $matches)) {
        $collection->push((object) ['name' => $matches[1], 'priority' => (integer) $matches[2]]);
      }
    }
    return $collection->sortBy('priority');
  }
  /**
   * Get the agent state id attribute from 'aics' or 'as' attributes
   *
   *  $this->attributes['aics'] is "Agent In Call State" and is published only when agent is in call
   *  $this->attributes['as'] is "Agent State" and is always published
   *
   *  $this->attributes['as']   ==  0  "Not Logged In"
   *  $this->attributes['aics'] ==  1  "Initialized"
   *  $this->attributes['as']   ==  2  "Released"
   *  $this->attributes['as']   ==  3  "Available"
   *
   *  $this->attributes['aics'] ==  5  "Wrap-Up"
   *  $this->attributes['aics'] ==  6  "In Call"
   *  $this->attributes['aics'] ==  7  "Ringing"
   *  $this->attributes['aics'] ==  8  "Preview"
   *  $this->attributes['aics'] ==  9  "On Hold"
   *  $this->attributes['as']   == 10  "Available In Call"
   *  $this->attributes['as']   == 11  "Released In Call"
   *
   * @return integer
   */
   public function getStateIdAttribute() {
     if($this->aics != '') {
       return (integer) $this->aics;
     }
     return (integer) $this->as;
   }
   /**
    * Get the agent state name
    *
    * @return string
    */
    public function getStateAttribute() {
      switch($this->state_id) {
        case(0):
          return 'Not Logged In';
        case(1):
          return 'Initialized';
        case(2):
          return 'Released ('.$this->rc.')';
        case(3):
          return 'Available';
        case(5):
          return 'Wrap-Up';
        case(6):
          return 'In Call';
        case(7):
          return 'Ringing';
        case(8):
          return 'Preview';
        case(9):
          return 'On Hold';
        case(10):
          return 'Available In Call';
        case(11):
          return 'Released In Call';
        default:
          return null;
      }
    }

    /** ==================
     *  Cosmo User permissions.
     *  ==================
     */
    /**
     * If agent is Administrator.
     *
     * @return boolean
     */
    public function getIsAdministratorAttribute() {
     return $this->isad == "True" ? true : false;
    }
    /**
     * If agent is Supervisor.
     *
     * @return boolean
     */
    public function getIsSupervisorAttribute() {
      return $this->issu == "True" ? true : false;
    }
    /**
     * If agent is just an Agent.
     *
     * @return boolean
     */
    public function getIsAgentAttribute() {
      return $this->isag == "True" ? true : false;
    }




  /** ==================
   *  Extrapolate and cast properties from the raw attributes.
   *  ==================
   */
  /**
   * Get the agent online state.
   *
   * @return boolean
   */
  public function getIsOnlineAttribute() {
    return !$this->is_offline;
  }
  /**
   * Get the agent offline state.
   *
   * @return boolean
   */
  public function getIsOfflineAttribute() {
    return $this->state_id == 0;
  }
   /**
    * Get the agent available state.
    *
    * @return boolean
    */
   public function getIsAvailableAttribute() {
     return $this->state_id == 3;
   }
   /**
    * Get the agent unavailable state.
    *
    * @return boolean|null
    */
   public function getIsUnavailableAttribute() {
     if( $this->is_offline ) {
       return null;
     }

     return !$this->is_available;
   }
  /**
   * If the agent is active (on a call or chat).
   *
   * @return boolean
   */
  public function getIsActiveAttribute() {
    return (boolean) $this->aics != 0;
  }
  /**
   * Get the agent inactive state.
   *
   * @return boolean|null
   */
  public function getIsInactiveAttribute() {
    if( $this->is_offline ) {
      return null;
    }

    return ! $this->is_active;
  }



  /**
   * Retun a dialog class from the current agent state.
   *
   * @return boolean|null
   */
  public function getDialogAttribute() {
    return new Dialog($this);
  }
}
