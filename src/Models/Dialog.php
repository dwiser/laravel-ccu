<?php

namespace IONOS\CCU\Models;

use Carbon\Carbon;
use Sabre\Xml\Reader;
use Jenssegers\Model\Model;
use Illuminate\Support\Collection;

class Dialog extends Model
{
  /**
   * The attributes that should be visible in arrays.
   *
   * @var array
   */
  protected $visible = [
    'id', 'agent_id', 'type', 'queue', 'answered_at', 'duration', 'tenant'
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'cct' => 'string',
    'cq' => 'string'
  ];
  /**
   * The accessors to append to the model's array form.
   *
   * @var array
   */
  protected $appends = [
    'type', 'agent_id', 'queue', 'answered_at', 'duration'
  ];



  /**
   * Construct a Dialog class.
   *
   * @param  Array $attributes
   * @return void
   */
  public function __construct(Agent $agent)
  {
    if( $agent->aics != 0 ) {
      $attributes = [
        'exists' => true,
        'agent' => $agent,
        'cct' => $agent->cct,
        'cq' => $agent->cq,
        'tp' => $agent->tp,
        'tenant' => $agent->tenant,
        'updated_at' => $agent->updated_at
      ];
    } else {
      $attributes['exists'] = false;
    }

    parent::__construct($attributes);
  }


  /**
   * Get the queue id.
   *  It is not currently possible to obtain a call ID.
   *
   * @return null
   */
  public function getIdAttribute()
  {
    return null;
  }
  /**
   * Get the agent id of this dialog.
   *
   * @return integer
   */
  public function getAgentIdAttribute()
  {
    return optional($this->agent)->id;
  }
  /**
   * Get the agent Dialog Type attribute.
   *
   * @return string|null
   */
  public function getTypeAttribute() {
    if( $this->exists ) {
      switch($this->cct) {
        case(0):
          return 'Internet';
        case(1):
          return 'Not Used';
        case(2):
          return 'Message';
        case(3):
          return 'VOIP';
        case(4):
          return 'Call Back';
        case(5):
          return 'Voicemail';
        case(6):
          return 'Multiple';
        default:
          return null;
      }
    }

    return null;
  }
  /**
   * Get the queue of the current dialog.
   *
   * @return string|null
   */
  public function getQueueAttribute() {
    return $this->cq;
  }
  /**
   * Get the time the dialog was answered.
   *
   * @return Carbon\Carbon|null
   */
  public function getAnsweredAtAttribute() {
    if( $this->exists ) {
      return $this->updated_at->copy()->subSeconds($this->tp);
    }

    return null;
  }
  /**
   * Get the time the agent has been in the current call.
   *
   * @return Carbon\CarbonInterval|null
   */
  public function getDurationAttribute() {
    if( $this->exists ) {
      return Carbon::now()->diffAsCarbonInterval(Carbon::now()->copy()->addSeconds($this->tp, false));
    }

    return null;
  }
  /**
   * If the current dialog is a chat.
   *
   * @return boolean|null
   */
  public function getIsChatAttribute() {
    return $this->cct == 0;
  }
  /**
   * If the current dialog is a call.
   *
   * @return boolean|null
   */
  public function getIsCallAttribute() {
    return $this->cct == 3;
  }
}
