<?php

namespace IONOS\CCU\Models;

use Sabre\Xml\Reader;
use Jenssegers\Model\Model;
use Illuminate\Support\Collection;

class Group extends Model
{
  /**
   * The attributes that should be visible in arrays.
   *
   * @var array
   */
  protected $visible = [
    'id', 'name', 'agents', 'queues', 'updated_at', 'tenant'
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'name' => 'string',
  ];
  /**
   * The accessors to append to the model's array form.
   *
   * @var array
   */
  protected $appends = [
    'id', 'name'
  ];



  /**
   * Construct a Group class.
   *
   * @param  Reader $reader
   * @return void
   */
  public function __construct(Reader $reader)
  {
    parent::__construct($reader->parseAttributes());
    $this->agents = new Collection;
    $this->queues = new Collection;

    $children = $reader->parseInnerTree();

    if( !is_null($children) ) {
      foreach($children as $child) {
        if ($child['value'] instanceof Queue) {
          $this->queues->push($child['value']);
        }
        if ($child['value'] instanceof Agent) {
          $this->agents->push($child['value']);
        }
      }
    }
  }


  /**
   * Get the group id.
   *
   * @return string
   */
  public function getIdAttribute()
  {
    return (int) $this->attributes['id'];
  }
  /**
   * Get the group name.
   *
   * @return string
   */
  public function getNameAttribute()
  {
    return (string) $this->attributes['n'];
  }
  /**
   * Get only the active Queues on this group.
   *
   * @return Collection
   */
  public function activeQueues()
  {
    return $this->queues->filter(function($queue) {
      return $queue->is_active;
    });
  }
}
