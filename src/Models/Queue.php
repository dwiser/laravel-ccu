<?php

namespace IONOS\CCU\Models;

use Carbon\Carbon;
use Sabre\Xml\Reader;
use Jenssegers\Model\Model;
use Illuminate\Support\Collection;

class Queue extends Model
{
  /**
   * The attributes that should be visible in arrays.
   *
   * @var array
   */
  protected $visible = [
    'id', 'name', 'is_active', 'calls_being_handled', 'calls_waiting', 'longest_wait', 'groups', 'updated_at', 'tenant'
  ];
  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'id' => 'integer',
    'n' => 'string',
    'pq' => 'boolean'
  ];
  /**
   * The accessors to append to the model's array form.
   *
   * @var array
   */
  protected $appends = [
    'id', 'name', 'is_active', 'calls_being_handled', 'calls_waiting', 'longest_wait',
  ];



  /**
   * Construct a Queue class.
   *
   * @param  Reader $reader
   * @return void
   */
  public function __construct(Reader $reader)
  {
    parent::__construct($reader->parseAttributes());
    $this->groups = new Collection;

    $children = $reader->parseInnerTree();

    if( !is_null($children) ) {
      foreach($children as $child) {
        if ($child['value'] instanceof Group) {
          $this->groups->push($child['value']);
        }
      }
    }
  }


  /**
   * Get the queue id.
   *
   * @return string
   */
  public function getIdAttribute()
  {
    return (int) $this->attributes['id'];
  }
  /**
   * Get the queue name.
   *
   * @return string
   */
  public function getNameAttribute()
  {
    return (string) $this->n;
  }
  /**
   * If the queue is active.
   *  Note: Only exists when returned as a subset of a group search. When it doesn't
   *  exist, because it's part of a queue search, always return true.
   *
   * @return boolean
   */
  public function getIsActiveAttribute()
  {
    return $this->pq ?: true;
  }



  /** ==================
   *  Handled Statistics.
   *  ==================
   */
  /**
   * Get the number of calls handled in this queue.
   * Note: Details are limited for this stat. I assume timeframe is the current hour.
   *
   * @return integer
   */
  public function getCallsHandledAttribute() {
    return (integer) $this->ch;
  }
  /**
   * Get the number of calls offered in this queue.
   * Note: Details are limited for this stat. I assume timeframe is the current hour.
   *
   * @return integer
   */
  public function getCallsOfferedAttribute() {
    return (integer) $this->co;
  }
  /**
   * Get the number of calls rejected in this queue.
   * Note: Details are limited for this stat. I assume timeframe is the current hour.
   *
   * @return integer
   */
  public function getCallsRejectedAttribute() {
    return (integer) $this->cr;
  }
  /**
   * Get the number of calls being handled in this queue.
   *
   * @return integer
   */
  public function getCallsBeingHandledAttribute() {
    return (integer) $this->cbh;
  }
  /**
   * Get the number of calls abandoned in this queue.
   * Note: Details are limited for this stat. I assume timeframe is the current hour.
   *
   * @return integer
   */
  public function getAbandonedCallsAttribute() {
    return (integer) $this->ca;
  }



  /** ==================
   *  Wait and time statistics.
   *  ==================
   */
  /**
   * Get the number of calls waiting in this queue.
   *
   * @return integer
   */
  public function getCallsWaitingAttribute() {
    return (integer) $this->cw;
  }
  /**
   * Get the average wait time in this queue.
   * Note: Details are limited for this stat. I assume timeframe is the current hour.
   *
   * @return Carbon\CarbonInterval
   */
  public function getAverageWaitAttribute() {


    return Carbon::now()->diffAsCarbonInterval(Carbon::now()->copy()->addSeconds($this->awt, false));
  }
  /**
   * Get the cumulative wait time in this queue.
   * Note: Details are limited for this stat. I assume timeframe is the current hour.
   *
   * @return Carbon\CarbonInterval
   */
  public function getCumulativeWaitAttribute() {
    return Carbon::now()->diffAsCarbonInterval(Carbon::now()->copy()->addSeconds($this->cwt, false));
  }
  /**
   * Get the longest Wait time in this queue.
   *
   * @return Carbon\CarbonInterval
   */
  public function getLongestWaitAttribute() {
    return Carbon::now()->diffAsCarbonInterval(Carbon::now()->copy()->addSeconds($this->wt, false));
  }
  /**
   * Get the average call time in this queue.
   * Note: Details are limited for this stat. I assume timeframe is the current hour.
   *
   * @return Carbon\CarbonInterval
   */
  public function getAverageCallAttribute() {
    return Carbon::now()->diffAsCarbonInterval(Carbon::now()->copy()->addSeconds($this->act, false));
  }


  /** ==================
   *  Overflowed calls.
   *  ==================
   */
   /**
    * Get the number of calls overflowed in to this queue.
    * Note: Details are limited for this stat. I assume timeframe is the current hour.
    *
    * @return integer
    */
   public function getCallsOverflowedInAttribute() {
     return (integer) $this->ofi;
   }
   /**
    * Get the number of calls overflowed out of this queue.
    * Note: Details are limited for this stat. I assume timeframe is the current hour.
    *
    * @return integer
    */
   public function getCallsOverflowedOutAttribute() {
     return (integer) $this->ofo;
   }
}
