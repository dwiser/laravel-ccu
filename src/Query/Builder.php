<?php

namespace IONOS\CCU\Query;

use InvalidArgumentException;
use IONOS\CCU\Models\ModelFactory;
use IONOS\CCU\Connections\Connection;

class Builder
{
  /**
   * Authenticated Connections
   *
   * @var Connection
   */
  protected $connection;



  /**
   * Construct a manager class for the Cosmo XML feed.
   *
   * @param  Connection $connection
   * @return void
   */
  public function __construct(Connection $connection)
  {
    $this->connection = $connection;
  }


  /**
   * Get connection for the query.
   *
   * @return \App\Services\Cosmo\Connection
   */
  public function getConnection()
  {
    return $this->connection;
  }
  /**
   * Get filters on connection.
   *
   * @return Collection
   */
  public function getFilters()
  {
    return $this->connection->getFilters();
  }
  /**
   * If filters have been set.
   *
   * @return boolean
   */
  public function hasFiltersSet()
  {
    return $this->connection->hasFiltersSet();
  }


  /**
   * Get all available data.
   *
   * @return Self
   */
  public function includeAll()
  {
    $this->includeAgents();
    $this->includeGroups();
    $this->includeQueues();

    return $this;
  }
  /**
   * Get all Agent data.
   *
   * @return Self
   */
  public function includeAgents()
  {
    return $this->includeAgentConfigurations()->includeAgentStates();
  }
  /**
   * Get Agent configuration data.
   *
   * @return Self
   */
  public function includeAgentConfigurations()
  {
    $this->connection->addFilter('AConfigs');

    return $this;
  }
  /**
   * Get Agent state data.
   *
   * @return Self
   */
  public function includeAgentStates()
  {
    $this->connection->addFilter('ACalls');

    return $this;
  }


  /**
   * Get all Group data.
   *
   * @return Self
   */
  public function includeGroups()
  {
    return $this->includeGroupConfigurations()->includeGroupStates();
  }
  /**
   * Get Group configuration data.
   *
   * @return Self
   */
  public function includeGroupConfigurations()
  {
    $this->connection->addFilter('GConfigs');

    return $this;
  }
  /**
   * Get Group state data.
   *
   * @return Self
   */
  public function includeGroupStates()
  {
    return $this;
  }




  /**
   * Get all Queue data.
   *
   * @return Self
   */
  public function includeQueues()
  {
    return $this->includeQueueConfigurations()->includeQueueStates();
  }
  /**
   * Get Queue configuration data.
   *
   * @return Self
   */
  public function includeQueueConfigurations()
  {
    $this->connection->addFilter('QConfigs');

    return $this;
  }
  /**
   * Get Queue state data.
   *
   * @return Self
   */
  public function includeQueueStates()
  {
    $this->connection->addFilter('QCalls');

    return $this;
  }





  /**
   * Get only Agent data.
   *
   * @return Self
   */
  public function onlyAgents()
  {
    $this->connection->setFilters(['AConfigs', 'ACalls']);

    return $this;
  }
  /**
   * Get only Group data.
   *
   * @return Self
   */
  public function onlyGroups()
  {
    $this->connection->setFilters(['GConfigs']);

    return $this;
  }
  /**
   * Get only Queue data.
   *
   * @return Self
   */
  public function onlyQueues()
  {
    $this->connection->setFilters(['QConfigs', 'QCalls']);

    return $this;
  }



  /**
   * Execute the request.
   *
   * @return mixed
   */
  public function execute()
  {
    return $this->connection->getResponse();
  }
}
