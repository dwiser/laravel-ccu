<?php

namespace IONOS\CCU\Query;

use IONOS\CCU\Models\Agent;
use IONOS\CCU\Models\Group;
use IONOS\CCU\Models\Queue;

use Sabre\Xml\Reader;
use Sabre\Xml\Service as XMLService;
use Illuminate\Support\Collection;

class Processor
{
  /**
   * Responses
   *
   * @var Array
   */
  protected $responses;



  /**
   * Construct a processor class.
   *
   * @param  array $responses
   * @return void
   */
  public function __construct(array $responses)
  {
    $this->responses = $responses;
    $this->service = new XMLService();
    $this->mapXMLElements();
  }


    /**
     * Add the XML Element Map to the XML Service.
     *
     * @return Void
     */
    public function mapXMLElements()
    {
      $this->service->elementMap = [
        // Map Agents
        '{}A' => function(Reader $reader) {
            return new Agent($reader);
          },
        // Map Groups
        '{}G' => function(Reader $reader) {
            return new Group($reader);
          },
        // Map Queues
        '{}Q' => function(Reader $reader) {
            return new Queue($reader);
          },
      ];
    }

  /**
   * Process the responses.
   *
   * @return Collection
   */
  public function process()
  {
    $agents = new Collection();
    $groups = new Collection();
    $queues = new Collection();
    $return = new Collection();

    // Loop through each endpoint
    foreach( $this->responses as $response ) {

      $data = $this->service->parse($response->getXML());

      foreach( $data as $type ) {

        if( !is_array($type['value']) ) {
          $objects = $data;
        } else {
          $objects = $type['value'];
        }


        foreach( $objects as $object) {

          $object['value']->setAttribute('tenant', $response->getTenant());
          $object['value']->setAttribute('updated_at', $response->getTimestamp());
          $class = get_class($object['value']);

          switch($class) {
            case Agent::class:
              $agents->push($object['value']);
              break;
            case Group::class:
              $groups->push($object['value']);
              break;
            case Queue::class:
              $queues->push($object['value']);
              break;
          }
        }
      }
    }

    $result = [];

    if( $agents->isNotEmpty() ) { $result['agents'] = $agents; }
    if( $groups->isNotEmpty() ) { $result['groups'] = $groups; }
    if( $queues->isNotEmpty() ) { $result['queues'] = $queues; }

    foreach( $result as $type => $collection ) {
      $return->put($type, $this->reduce($collection));
    }

    return $return->count() == 1 ? $return->first() : $return;
  }

  /**
   * Reduce to only unique values.
   * When the collection is a group of agents, group by uname since users may have
   * different id's in each tenant. For all other types, group by id.
   *
   * @param  array  $result
   * @return Collection
   */
  private function reduce(Collection $collection)
  {
    if( $collection->contains('uname', true) ) {
      $groups = $collection->groupBy('uname');
    } else {
      $groups = $collection->groupBy('id');
    }
    $response = new Collection;

    foreach( $groups as $item ) {

      if( $item->count() > 1 ) {

        $item = $item->reduce(function($previous, $current) {
          return is_null($previous) ? $current : $current->fill($previous->toArray());
        });

      } else {
        $item = $item->first();
      }

      $response->push($item);
    }

    return $response;
  }
}
