<?php

namespace IONOS\CCU\Query;

use InvalidArgumentException;
use IONOS\CCU\Query\Builder;
use IONOS\CCU\Query\Processor;

class SearchFactory
{
  /**
   * Authenticated Connections
   *
   * @var Array
   */
  protected $connections;
  /**
   * Prepared Builders
   *
   * @var Array
   */
  protected $builders;



  /**
   * Construct a manager class for the Cosmo XML feed.
   *
   * @param  array $connections
   * @return void
   */
  public function __construct(array $connections)
  {
    foreach( $connections as $name => $connection ) {
      $this->connections[$name] = $connection;
      $this->builders[$name] = new Builder($connection);
    }
  }




  /**
   * Returns all of the connections.
   *
   * @return array
   */
  public function getConnections()
  {
    return $this->connections;
  }
  /**
   * Returns all of the builders.
   *
   * @return array
   */
  public function getBuilders()
  {
    return $this->builders;
  }

  /**
   * Executes a search on all builders and compiles the results into a Collection.
   *
   * @return \Illuminate\Support\Collection|array
   */
  public function get()
  {
    foreach( $this->getBuilders() as $name => $builder ) {

      $responses[] = $builder->hasFiltersSet() ? $builder->execute() : $builder->includeAll()->execute();
    }

    return (new Processor($responses))->process();
  }



  /**
   * Returns all of the builders.
   *
   * @return array
   */
  public function agents()
  {
    foreach( $this->getBuilders() as $name => $builder ) {
      $builder->includeAgents();
    }

    return $this;
  }
  /**
   * Returns all of the builders.
   *
   * @return array
   */
  public function groups()
  {
    foreach( $this->getBuilders() as $name => $builder ) {
      $builder->includeGroups();
    }

    return $this;
  }
  /**
   * Returns all of the builders.
   *
   * @return array
   */
  public function queues()
  {
    foreach( $this->getBuilders() as $name => $builder ) {
      $builder->includeQueues();
    }

    return $this;
  }
}
