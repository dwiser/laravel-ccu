<?php

namespace IONOS\CCU;

use IONOS\CCU\Manager;
use IONOS\CCU\Connections\Provider;
use IONOS\CCU\Connections\Connection;

use Illuminate\Container\Container;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider as Service;

class ServiceProvider extends Service
{
  /**
    * Indicates if loading of the provider is deferred.
    *
    * @var bool
    */
  protected $defer = false;

  /**
   * Bootstrap services.
   *
   * @return void
   */
  public function boot()
  {
    $this->publishConfig();

    $this->loadMigrationsFrom(__DIR__.'/Migrations/');

    if ($this->app->runningInConsole()) {
      $this->registerCommands();
    }
  }

  /**
   * Register services.
   *
   * @return void
   */
  public function register()
  {
    $this->app->singleton(Manager::class, function (Container $app) {
      $config = $app->config->get('ccu', ['connections' => []]);

      return (new Manager())->addConnections($config['connections']);
    });


    $this->app->alias(Manager::class, 'ionos.ccu');
  }

  /**
   * Register commands.
   *
   * @return void
   */
  public function registerCommands()
  {
    $this->commands([
      \IONOS\CCU\Commands\Init::class,
      \IONOS\CCU\Commands\Sync::class,
      \IONOS\CCU\Commands\Clean::class,
      \IONOS\CCU\Commands\Archive::class
    ]);
  }

  /**
   * Publish the config file
   */
  protected function publishConfig()
  {
    $this->publishes([
      __DIR__.'/../config/config.php' => config_path('ccu.php'),
    ]);
  }
}
